import useFetch from './useFetch.jsx';
export default function ListarPersonas(){
	const[lista]=useFetch('http://127.0.0.1:2347/personas',sessionStorage.getItem('token'));
	return(
	<div>
		{lista.map((x,i)=><span key={i}>{x.nombre+" - "+x.celular}</span>)}
	</div>);
}