import React, { useState } from 'react';
import axios from 'axios';
export default function Login(){
	const[login,setLogin]=useState("");
	const[clave,setClave]=useState("");
	const aceptar=(e)=>{
		e.preventDefault();
		axios.post('http://127.0.0.1:2347/login',{login,clave})
		.then(({data})=>sessionStorage.setItem('token',data.token))
		.catch(console.log);
	}
	return(
		<form onSubmit={e=>aceptar(e)}>
			<div>
				nombre:<input value={login} onChange={evento=>setLogin(evento.target.value)}></input>
			</div>
			<div>
				clave:<input value={clave} onChange={e=>setClave(e.target.value)}></input>
			</div>
			<div>
				<button>Aceptar</button>
			</div>
		</form>
	);
}