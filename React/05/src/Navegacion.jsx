import {BrowserRouter,Link,Routes,Route} from 'react-router-dom';
import Home from './Home';
import SignUp from './SignUp';
import Login from './Login';
import ListarPersonas from './ListarPersonas';
export default function Navegacion(){
	return(
		<BrowserRouter>
			<ul>
				<li>
					<Link to="/">Home</Link>
				</li>
				<li>
					<Link to="/login/signup">Registrarse</Link>
				</li>
				<li>
					<Link to="/login">Login</Link>
				</li>
				<li>
					<Link to="/personas">Listar personas</Link>
				</li>
			</ul>
			<Routes>
				<Route path="/" element={<Home/>}/>
				<Route path="/login/signup" element={<SignUp/>}/>
				<Route path="/login" element={<Login/>}/>
				<Route path="/personas" element={<ListarPersonas/>}/>
			</Routes>
		</BrowserRouter>
	);
}