import React from 'react';
export default function useFetch(url,token){
	const[datos,setDatos]=React.useState([]);
	React.useEffect(()=>{
		fetch(url,{method:'GET',headers:{
			"Authorization":"Bearer "+token
		}})
		.then(respuesta=>respuesta.json())
		.catch(e=>{throw e})
		.then(datos=>setDatos(datos));
	},[url,token]);
	return[datos];
}