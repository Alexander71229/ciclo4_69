import React, { useState } from 'react';
import axios from 'axios';
export default function SignUp(){
	const[login,setLogin]=useState("");
	const[clave,setClave]=useState("");
	const aceptar=(e)=>{
		e.preventDefault();
		axios.post('http://127.0.0.1:2347/login/signup',{login,clave})
		.then(console.log)
		.catch(console.log);
		/*fetch('http://127.0.0.1:2347/login/signup',{
			method:'POST',
			headers:{'Content-type': 'application/json; charset=UTF-8'},
			body:JSON.stringify({login,clave})
		}).catch(console.log).then(console.log);*/
	}
	return(
		<form onSubmit={e=>aceptar(e)}>
			<div>
				nombre:<input value={login} onChange={evento=>setLogin(evento.target.value)}></input>
			</div>
			<div>
				clave:<input value={clave} onChange={e=>setClave(e.target.value)}></input>
			</div>
			<div>
				<button>Aceptar</button>
			</div>
		</form>
	);
}