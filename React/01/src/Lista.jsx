import React from 'react';
import Contador from './Contador';
class Lista2 extends React.Component{
	state={};
	render(){
		return<></>;
	}
}
export default function Lista(){
	
	var [lista,actualizarLista]=React.useState(['Valor 1','Valor 2','Valor 3']);
	const agregar=()=>{
		actualizarLista([...lista,'Valor '+(lista.length+1)]);
	}
	return(
		<div>
			<button onClick={agregar}>Adicionar a la lista</button>
			<button onClick={()=>{actualizarLista([...lista].filter((x,indice)=>indice!=lista.length-1))}}>Eliminar</button>
			{lista.map((elemento,indice)=>
			<Contador key={indice} cuentaInicial={indice}/>
			)}
		</div>
	);
}