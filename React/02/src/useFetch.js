import React from 'react';
export default function useFetch(url){
	const[datos,setDatos]=React.useState([]);
	React.useEffect(()=>{
		fetch('https://jsonplaceholder.typicode.com/todos/1')
			.then(respuesta=>respuesta.json())
				.then(datos=>setDatos(lista=>setDatos([...lista,datos])));
	},[url]);
	return[datos];
}