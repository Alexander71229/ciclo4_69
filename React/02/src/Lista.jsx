import React from 'react';
import Titulo from './Titulo';
import useFetch from './useFetch';
export default function Lista(){
	const[lista]=useFetch('https://jsonplaceholder.typicode.com/todos/1');
	return(
		<>
			<div>Lista desde el API</div>
			{lista.map(e=><Titulo id={e.id} title={e.title}/>)}
		</>
	);
}