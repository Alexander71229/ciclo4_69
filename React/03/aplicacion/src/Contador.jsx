import { useEffect, useState, useRef} from "react";

export default function Contador(){
	const[cuenta,setCuenta]=useState(0);
	var cantidadActualizaciones=useRef(0);
	useEffect(()=>{
		cantidadActualizaciones.current=cantidadActualizaciones.current+1;
		console.log('Cantidad Actualizaciones:'+cantidadActualizaciones.current);
		setTimeout(()=>{
			setCuenta(cuenta+1);
		},1000);
	},[cuenta]);
	return(
		<div>
			<div>{cuenta}</div>
		</div>
	);
}