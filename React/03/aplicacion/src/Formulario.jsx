import './Formulario.css';
import React from 'react';
export default function Formulario(){
	const referencia=React.useRef(null);
	const funcionSubmit=(evento)=>{
		evento.preventDefault();
		console.log(referencia.current.value);
	}
	return(
		<form onSubmit={evento=>{funcionSubmit(evento)}}>
			<div className='rojo'>
				Usuario
				<input name='usuario' ref={referencia} type='text'/>
			</div>
			<div style={{color:'red'}}>
				Contraseña
				<input/>
			</div>
			<div>
				<input type='submit' value='Aceptar'/>
			</div>
		</form>
	);
};