import { useEffect, useState } from "react";
import axios from "axios";
import Planeta from "./Planeta";
export default function Lista(){
	const[datos,setDatos]=useState([]);//const arreglo=useState([]);const datos=arreglo[0];const setDatos=arreglo[1];
	useEffect(()=>{
		axios.get('https://swapi.dev/api/planets/')
		.then(({data})=>{console.log(data.results);setDatos(data.results);}).catch(console.error);
	},[]);
	return(
		<div>
			{datos.map((planeta,indice)=><Planeta key={indice} datos={planeta}/>)}
		</div>
	);
}
