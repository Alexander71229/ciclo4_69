import { useState } from "react";
import axios from "axios";
export default function Login(){
	const[login,setLogin]=useState("");
	const[clave,setClave]=useState("");
	const aceptar=(evento)=>{
		evento.preventDefault();
		axios.post("http://127.0.0.1:2347/login/signup",{login,clave}) //{login:login,clave:clave}
		.then(({data})=>{
			if(data.r===0){
				alert('Registrado');
			}else{
				alert('Error al registrarse');
			}
		});
	}
	return(
		<form onSubmit={aceptar}>
			<fieldset>
				<legend>Registro</legend>
				<div>
					login:<input value={login} onChange={e=>setLogin(e.target.value)}/>
				</div>
				<div>
					clave:<input type='password' value={clave} onChange={e=>setClave(e.target.value)}/>
				</div>
				<div>
					<button>Aceptar</button>
				</div>
			</fieldset>
		</form>
	);
}