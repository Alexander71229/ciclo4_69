import { BrowserRouter,Routes,Route } from "react-router-dom";
import Lista from "./components/Lista.jsx";
import Menu from "./Menu.js";
import Login from "./components/Login.jsx";
export default function Contenedor(){
	return(
		<BrowserRouter>
			<Routes>
				<Route path="/" element={<Menu/>}/>
				<Route path="/planetas" element={<Lista/>}/>
				<Route path="/login" element={<Login/>}/>
			</Routes>
		</BrowserRouter>
	);
}