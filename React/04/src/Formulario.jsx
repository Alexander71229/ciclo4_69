import React, { useEffect, useState } from 'react';
export default function Formulario(){
	const[nombre,setNombre]=useState("");
	const[telefono,setTelefono]=useState("");
	useEffect(()=>{
		//console.log("nombre:"+nombre)
	},[nombre]);
	const aceptar=(e)=>{
		e.preventDefault();
		console.log("Guardando...");
		console.log("Nombre:"+nombre);
		console.log("Teléfono:"+telefono);
	}
	return(
		<form onSubmit={e=>aceptar(e)}>
			<div>
				nombre:<input value={nombre} onChange={evento=>setNombre(evento.target.value)}></input>
			</div>
			<div>
				teléfono:<input value={telefono} onChange={e=>setTelefono(e.target.value)}></input>
			</div>
			<div>
				<button>Aceptar</button>
			</div>
		</form>
	);
}