import {BrowserRouter,Link,Routes,Route} from 'react-router-dom';
import Home from './Home';
import Formulario from './Formulario';
export default function Navegacion(){
	return(
		<BrowserRouter>
			<ul>
				<li>
					<Link to="/">Home</Link>
				</li>
				<li>
					<Link to="/formulario">Formulario</Link>
				</li>
			</ul>
			<Routes>
				<Route path="/" element={<Home/>}/>
				<Route path="/formulario" element={<Formulario/>}/>
			</Routes>
		</BrowserRouter>
	);
}