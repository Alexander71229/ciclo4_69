var productosAPI=null;
var productosConDepreciacion=[];
async function mostrarProductoMayor(){
	let response=await fetch("https://misiontic2022upb.vercel.app/api/logistics/products");
	console.log(response);
	productosAPI=await response.json();
	console.log(productosAPI);
	var copia=[...productosAPI];
	console.log(copia);
	var imax=0;
	var max=-1;
	for(var i=0;i<copia.length;i++){
		copia[i].precioDepreciado=calcularDepreciacionNIIF(copia.precioInicial,copia.precioFinal,copia.vidaUtil,copia.periodoConsultado);
		productosConDepreciacion.push(copia[i]);
		if(copia[i].precioDepreciado>max){
			max=copia[i].precioDepreciado;
			imax=i;
		}
	}
	console.log(imax);
	return [copia[imax]];
}
module.exports.mostrarProductoMayor=mostrarProductoMayor;
module.exports.productosAPI=productosAPI;
module.exports.productosConDepreciacion=productosConDepreciacion;