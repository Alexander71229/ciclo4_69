const calcularDepreciacionNIIF=(precioInicial,precioFinal,vidaUtil,numeroPeriodoAconsultar)=>{
	if(vidaUtil<=0){
		return 0;
	}
	if(numeroPeriodoAconsultar<0){
		return precioInicial;
	}
	if(numeroPeriodoAconsultar>vidaUtil){
		return precioFinal;
	}
	var precio=(precioFinal-precioInicial)*numeroPeriodoAconsultar/vidaUtil+precioInicial;
	return precio;
};
const calcularDepreciacionNIIFEnDolares=(precioInicial,precioFinal,vidaUtil,numeroPeriodoAconsultar)=>{
	if(precioInicial<0){
		throw "error";
	}
	return calcularDepreciacionNIIF(precioInicial,precioFinal,vidaUtil,numeroPeriodoAconsultar)/3778;
};
module.exports.calcularDepreciacionNIIF=calcularDepreciacionNIIF;
module.exports.calcularDepreciacionNIIFEnDolares=calcularDepreciacionNIIFEnDolares;