const express=require('express'); //importación usada en ES5. En ES6 se puede usar import express from 'express';
const app=express();
app.use(express.urlencoded({extended:false}));//req.body=parseo de ?variable1=valor1&variable1=valor2&variable2=valor3...
app.use(express.json());//req.body=json
/*app.use((req,res,next)=>{ //middleware personalizado.
next(); //pasa la responsabilidad a la siguiente función middleware
});*/
app.get("/api/logistics/products",(req,res)=>{
	res.send(products);
});
app.post("/api/logistics/products",(req,res)=>{
	products.push(req.body);
	res.send(products);
});
module.exports=app; //Exportar variable por defecto usando ES5. En ES6 se puede escribir como export default app 