const mgdb=require('../BaseDeDatos/conexion.js');
const nombreColeccion='personas';
mgdb.model(nombreColeccion,{
	"id":{type:String,unique:true},
	"nombre":String,
	"celular":String
});
module.exports=mgdb.model(nombreColeccion);