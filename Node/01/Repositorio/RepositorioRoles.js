const mgdb=require('../BaseDeDatos/conexion.js');
const nombreColeccion='roles';
mgdb.model(nombreColeccion,{
	"nombre":{type:String,unique:true},
	"permisos":[{type:String}]
});
module.exports=mgdb.model(nombreColeccion);