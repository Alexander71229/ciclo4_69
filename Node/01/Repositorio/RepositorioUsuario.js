const Schema=require('mongoose');
const mgdb=require('../BaseDeDatos/conexion.js');
const nombreColeccion='usuario';
mgdb.model(nombreColeccion,{
	"login":{type:String,unique:true},
	"clave":String,
	roles:[{ref:"roles",type:Schema.Types.ObjectId}]
});
module.exports=mgdb.model(nombreColeccion);