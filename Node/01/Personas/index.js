const express=require('express');
const repositorio=require('../Repositorio/RepositorioPersona.js');
const enrutador=express.Router();
enrutador.route("/crear").post((req,res)=>{
	repositorio.create(req.body).then(()=>res.send({respuesta:0})).catch((e)=>res.send({respuesta:1,error:e}));
});
enrutador.route("/").get((req,res)=>{
	repositorio.find({}).then(datos=>res.send(datos)).catch((e)=>res.send({respuesta:1,error:e}));
});
enrutador.route("/:id").get((req,res)=>{
	repositorio.findById(req.params.id).then(datos=>res.send(datos)).catch((e)=>res.send({respuesta:1,error:e}));
});
enrutador.route("/actualizar").put((req,res)=>{
	repositorio.findByIdAndUpdate(req.body,req.body).then(()=>res.send({respuesta:0})).catch((e)=>res.send({respuesta:1,error:e}));
});
enrutador.route("/eliminar").delete((req,res)=>{
	repositorio.deleteOne(req.body).then(()=>res.send({respuesta:0})).catch((e)=>res.send({respuesta:1,error:e}));
});
module.exports=enrutador;